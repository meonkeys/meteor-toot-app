# toot-app

Demo [Meteor](https://www.meteor.com) mobile app forked from [here](https://github.com/meonkeys/cordova-file-test).

## Try it out

1. Clone this repository.
1. [Install prerequisites](https://guide.meteor.com/mobile.html#installing-prerequisites-android).
1. Enable USB debugging on the device.
1. Install and run app with `meteor run android-device`.

## Troubleshooting

### Cannot run on Android device

I tried `meteor run android-device` and was seeing errors like this:

    Status of the requirements:
    ✓ Java JDK
    ✓ Android SDK
    ✗ Android target: ...snipped...
    ✗ Gradle: ...snipped...

The fix was to make sure the SDK Platform with API Level "26" is installed, then run:

```bash
meteor remove-platform android
meteor add-platform android
```

## Copyright, License

Copyright (C) 2018 Adam Monsen

MIT license.
