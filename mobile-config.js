'use strict';

App.info({
  id: 'com.adammonsen.app',
  name: 'TootPro™',
  description: 'Simple offline sound effect app',
  author: 'Adam Monsen',
  email: 'haircut@gmail.com',
  website: 'https://adammonsen.com'
});
